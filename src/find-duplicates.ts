export interface Transaction {
  id: number,
  source: string,
  target: string,
  amount: number,
  description: string
}

const comparator = (obj: Transaction, list: Array<Transaction>): Array<any> => {
  const { id, ...comp1 } = obj;
  return list.filter(item => JSON.stringify(item) == JSON.stringify({ id: item.id, ...comp1 }))
    .map(item => item.id);
}

/**
 * Find duplicate transactions
 * Given a list of transactions, find and return duplicate transactions. There can be more than one duplicate transactions.
 * A transaction can be called duplicate if it has the same `source`, `target`, `amount` and `description`.
 * 
 * This is how a transaction looks like.
 * 
 * {
 *   id: 1,
 *   source: 'A',
 *   target: 'B',
 *   amount: 300,
 *   description: 'tikkie'
 * }
 * 
 * Note:
 * - Create additional functions if required.
 * - Follow proper coding conventions and best practices.
 * - Make sure existing unit test passes.
 * - Write further unit tests to cover maximum code.
 *  
 * 
 * @param transactions List of transactions
 * @returns {Transaction[]} List of duplicate transactions
 */
export function findDuplicateTransactions(transactions: Transaction[]): Transaction[] {

  if (transactions.length <= 1)
    return [];

  const lookup = transactions.reduce((acc: Array<number>, curr: Transaction) => {
    if (acc.indexOf(curr.id) == -1) {
      const afterComparison = comparator(curr, transactions)
      if (afterComparison.length > 1)
        acc = [...acc, ...afterComparison];
    }
    return acc;
  }, []);
  return transactions.filter(item => lookup.indexOf(item.id) > -1);
}